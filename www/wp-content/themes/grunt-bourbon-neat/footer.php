<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package some_like_it_neat
 */
?>
		<?php tha_content_bottom(); ?>

		</main><!-- #main -->

		<?php tha_content_after(); ?>

		<?php tha_footer_before(); ?>
    <div class="pre-footer">
      <?php
        wp_nav_menu(
          array(
            'theme_location' => 'primary-navigation',
            'menu_class' => 'main-site-navigation',
          )
        );
      ?>
    </div>
		<footer id="colophon" class="site-footer wrap" role="contentinfo" itemscope="itemscope" itemtype="http://schema.org/WPFooter">
      <div class="container">
		<?php tha_footer_top(); ?>

			<section class="site-info">
        <div itemscope itemtype="http://schema.org/LocalBusiness">
          <h3 itemprop="name">Alyce Osbourne</h3>
          <a href="mailto:shikaruuabe@gmail.com" title="Email Alyce - shikaruuabe@gmail.com" itemprop="email">shikaruuabe@gmail.com</a>
          <a href="tel:07497625216" title="Phone Alyce - 07497625216" itemprop="telephone">+44 (0) 74976 25216</a>
        </div>
			</section><!-- .site-info -->
      </div>
		</footer><!-- #colophon -->

		<?php tha_footer_after(); ?>

	</div><!-- .wrap -->

</div><!-- #page -->

<?php tha_body_bottom(); ?>

<?php wp_footer(); ?>

</body>
</html>
