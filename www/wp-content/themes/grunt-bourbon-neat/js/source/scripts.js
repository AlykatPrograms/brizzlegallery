/**
 * @file
 * A JavaScript file for the theme.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - https://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
(function ($) {

  $(window).ready(function () {
    // Execute code once the window is ready.
  });

  $(window).load(function () {
    // Execute code once the window is fully loaded.
  });

  $(window).resize(function () {
    // Execute code when the window is resized.
  });

  $(window).scroll(function () {
    // Execute code when the window scrolls.

  });

  $(document).ready(function () {
    // Execute. code once the DOM is ready.
    navigation('header .menu-button', 'header .main-site-navigation');
  });
  
  /*
   * function writeAllFunctionsHere(){
   *  return 'Keep all functions down here';
   * }
   */
  
  var mobileWidth = 465;
  
  function navigation(button, navigation){
    
    $(navigation).prepend('<a class="close-menu">Close</a>');
     
    $(button).click(function(){    
      if ( $('body').hasClass('menu-open') ){  
        $('body').removeClass('menu-open');
      } else {
        $('body').addClass('menu-open');
        
        $(document).click(function(e){
          if ( !$(e.target).hasClass('main-site-navigation') && !$(e.target).parent().hasClass('main-site-navigation') ){
            $('body').removeClass('menu-open');
          }
        });
      }
    });
    
    

    $(navigation).find('.close-menu').click(function(){
      $('body').removeClass('menu-open');
    });
  }
  
})(jQuery);