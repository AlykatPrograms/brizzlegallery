module.exports = function(grunt) {
    //styleguide = require('sc5-styleguide');
    // This is where we configure each task that we'd like to run.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        watch: {
            // This is where we set up all the tasks we'd like grunt to watch for changes.
            scripts: {
                files: ['./js/source/*.js'],
                tasks: ['uglify'],
                options: {
                    spawn: false,
                    nonull: true
                },
            },
            images: {
                files: ['./images/source/**/*'],
                tasks: ['imagemin'],
                options: {
                    spawn: false,
                    nonull: true,
                }
            },
            css: {
                files: ['./sass/*.scss', './sass/**/*.scss'],
                tasks: ['sass_globbing', 'sass'], //, 'gulp:styleguide-generate', 'gulp:styleguide-applystyles', 'copy'],
                options: {
                    spawn: false,
                    nonull: true,
                    outputStyle: 'compact',
                    sourceMap: true,
                }
            },
            sprites: {
              files: ['./images/optimized/sprite-elements/*.png'],
              tasks: ['sprite'],
              options: {
                  spawn: false,
                  nonull: true,
              }
            }
            
	    
            
        },
        uglify: {
            // This is for minifying all of our scripts.
            options: {
                sourceMap: true,
                mangle: false
            },
            my_target: {
                files: [{
                    /*expand: true,
                    cwd: 'js/source',
                    src: '*.js',
                    dest: 'js/build',
                    nonull: true*/
                    'js/build/scripts.js': ['js/source/*.js']
                }]
            }
        },
        imagemin: {
            // This will optimize all of our images for the web.
            dynamic: {
                files: [{
                    expand: true,
                    cwd: './images/source/',
                    src: ['**/*.png' ],
                    dest: './images/optimized/',
                    nonull: true
                }]
            }
        },
        sass: {
            // This will compile all of our sass files
            // Additional configuration options can be found at https://github.com/gruntjs/grunt-contrib-sass
            dist: {
                options: {
                    outputStyle: 'compressed', // This controls the compiled css and can be changed to nested, compact or compressed
                    require: 'sass-globbing',
                    includePaths: require('node-neat').includePaths,
                    sourceMap: true,
                },
                files: [{
                  './css/style.css': './sass/style.scss',
                }]
            }
        },
        sprite:{
          all: {
            src: './images/optimized/sprite-elements/*.png',
            retinaSrcFilter: './images/optimized/sprite-elements/*@2x.png',
            dest: './images/source/sprites/spritesheet.png',
            retinaDest: './images/source/sprites/spritesheet@2x.png',
            imgPath: '../images/optimized/sprites/spritesheet.png?'+ (new Date().getTime()),
            destCss: './sass/sprites.scss',
            cssTemplate: './scss-templates/scss_maps.template.moustache',
          }
        },
        /*
        copy: {
          
          css: {
            src: 'css/*',
            dest: 'styleguide/',
            expand: true,
          },
          
          fonts: {
            src: 'fonts/*',
            dest: 'styleguide/',
            expand: true,
          },
          images: {
            src: 'images/optimized/**',
            dest: 'styleguide/',
            expand: true,
          }
        

        },
        */
        sass_globbing: {
          my_target: {
            files: {
              'sass/_design.scss': 'sass/design/**/*.scss',           
            },
          },
          options: {
            useSingleQuotes: false
          }
        },
        webfont: {
          icons: {
            src: 'images/source/icons/*.svg',
            dest: 'fonts',
            destCss: 'sass',
            options: {
              types: 'eot,woff2,woff,ttf,svg',
              stylesheets: ['scss'],
              htmlDemo: false, //Comment out to preview icons
              autohint: false,
              customOutputs: [{
                template: 'scss-templates/scss_iconfont.template.moustache',
                dest: 'sass/_icons.scss'
              }]
            }
          }
        },
      });
    // This is where we tell Grunt we plan to use this plug-in.
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-sass-globbing');
     // Load in `grunt-spritesmith`
    grunt.loadNpmTasks('grunt-spritesmith');
    grunt.loadNpmTasks('grunt-webfont');
    // Now that we've loaded the package.json and the node_modules we set the base path
    // for the actual execution of the tasks
    //grunt.file.setBase('../')
    // This is where we tell Grunt what to do when we type "grunt" into the terminal.
    // Note. if you'd like to run and of the tasks individually you can do so by typing 'grunt mytaskname' alternatively
    // you can type 'grunt watch' to automatically track your files for changes.
    grunt.registerTask('default', [ 'imagemin', 'sprite', 'webfont', 'watch', 'sass_globbing']);
};
