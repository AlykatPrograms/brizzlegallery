<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package some_like_it_neat
 */
?>
<?php tha_entry_before(); ?>


<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> itemscope itemType="http://schema.org/WebPage">
	<?php tha_entry_top(); ?>
  
  <?php echo do_shortcode('[galitem]');?>
  
	<div class="entry-content" itemprop="mainContentOfPage">
    <?php the_content(); ?>
	</div><!-- .entry-content -->
	<?php tha_entry_bottom(); ?>
</article><!-- #post-## -->
