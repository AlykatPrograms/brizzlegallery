<?php
/**
 * Plugin Name: Gallery Item
 * Plugin URI: https://www.nameless.co.uk
 * Description: Registers post type for Gallery
 * Version: 1.0.0
 * Author: Ross Tovey/Modified to purpose By AlyKat
 * Author URI: https://www.linkedin.com/in/ross-tovey-9091b2a4
 * License: Copyright Nameless 2017
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}


//Create the new posttype
require_once( dirname( __FILE__ ) . '/lib/post-type-galitem.php' );

//Create the widget for them
require_once( dirname( __FILE__ ) . '/lib/galitem-shortcode.php' );
