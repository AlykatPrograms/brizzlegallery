<?php

function galitem_handler() {
  $query_args = array(
    'post_type' => 'galleryitem',
    'post_status' => 'publish',
    'posts_per_page' => 5,
    'orderby' => 'post_date');

  $my_query = new WP_Query($query_args);

  
   
  
  if ($my_query->have_posts()) {
      $output = '<div class="galpost"><ul>';
      
    while ($my_query->have_posts()) {
      $my_query->the_post();
      $title = get_the_title();
      $image = get_the_post_thumbnail_url();
      $content = get_the_content();
      $postid = get_the_id();
      $output .= '<li class="gal'.$postid.'"><div class="galleryimg"><img src="'.$image.'"></div><div class="gallerypost"><h2>'.$title.'</h2><b>'.$content.'</b></div></li>';
    }
    
    $output .= '</ul></div>';
  
    return $output;
  }
  
  wp_reset_postdata();
  
  
}

add_shortcode( 'galitem', 'galitem_handler' );