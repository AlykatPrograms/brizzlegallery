<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

function galitem_create_post_type() {
  register_post_type( 'galleryitem',
    array(
      'labels' => array(
        'name' => __( 'Gallery Item' ),
        'singular_name' => __( 'Gallery Item' )
      ),
      'public' => true,
      'has_archive' => true,
      'rewrite' => array('slug' => 'galitem'),
      'supports' => array('thumbnail', 'editor', 'title'),
    )
  );
}

add_action( 'init', 'galitem_create_post_type' );
